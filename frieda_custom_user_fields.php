<?php
/*
 * Plugin Name: Frieda & Friedrich Custom User Fields
 * Author: Nils Larsen
 */

 //1. Add a new form element...
add_action( 'register_form', 'frieda_custom_registration_mail_register_form' );
function frieda_custom_registration_mail_register_form() {
    
    $first_name = ( ! empty( $_POST['first_name'] ) ) ? sanitize_text_field( $_POST['first_name'] ) : '';
    $last_name = ( ! empty( $_POST['last_name'] ) ) ? sanitize_text_field( $_POST['last_name'] ) : '';
    $phone = ( !empty( $_POST['phone'] ) ) ? sanitize_text_field( $_POST['phone'] ) : '';
    $address = ( !empty( $_POST['address'] ) ) ? sanitize_text_field( $_POST['address'] ) : '';
    $terms_accepted = ( !empty( $_POST['terms_accepted'] ) ) ? sanitize_text_field( $_POST['terms_accepted'] ) : '';
    $honigtopf = ( !empty( $_POST['honigtopf'] ) ) ? sanitize_text_field( $_POST['honigtopf'] ) : '';

    printf( <<<END
                        
                        <p>
                            <label for="first_name">Vorname</label>
                            <input type="text" name="first_name" id="first_name" class="input" value="%s" size="25" autocomplete="given-name">
                        </p>
            END, esc_attr($first_name) );

    printf( <<<END
                        
                        <p>
                            <label for="last_name">Nachname</label>
                            <input type="text" name="last_name" id="last_name" class="input" value="%s" size="25" autocomplete="family-name">
                        </p>
            END, esc_attr($last_name) );
    
    printf( <<<END
                        
                        <p>
                            <label for="phone">Telefon</label>
                            <input type="tel" name="phone" id="phone" class="input" value="%s" size="25" autocomplete="tel">
                        </p>
            END, esc_attr($phone) );

    printf( <<<END
                        
                        <p>
                            <label for="address">Adresse (Straße, Hausnummer, PLZ, Ort)</label>
                            <input type="text" name="address" id="address" class="input" value="%s" size="25" autocomplete="address-line1"/>
                        </p>
            END, esc_attr($address) );
            
    $terms_accepted_checked_html = ($terms_accepted == "yes") ? " checked" : "";
    printf( <<<END
                        <p>
                            <label for="terms_accepted">Nutzungsbedingungen</label><br>
                            <input type="checkbox" name="terms_accepted" id="terms_accepted" value="yes"$terms_accepted_checked_html>Ich akzeptiere die <a href="/Nutzungsbedingungen.pdf" target="_blank">Nutzungsbedingungen</a> und die <a href="/Datenschutzerklaerung.pdf" target="_blank">Datenschutzerklärung</a><br>
                        </p>
                    

            END);
            
        
    printf( <<<END
                        
                        <br><p>
                            <label for="honigtopf">Bitte gib "Lastenrad" in das untenstehende Feld ein</label>
                            <input type="text" name="honigtopf" id="honigtopf" class="input" value="%s" size="25">
                        </p>
                        
            END, esc_attr($honigtopf));

    }

/* Perform validation of new user registration
 * CROSSCHECK with frieda_custom_registration_mail_user_profile_update_errors
 */
add_filter( 'registration_errors', 'frieda_custom_registration_mail_registration_errors', 10, 3 );
function frieda_custom_registration_mail_registration_errors( $errors, $sanitized_user_login, $user_email ) {

    if (!frieda_custom_registration_mail_validate_name($_POST['first_name'] )) { 
        $errors->add( 'first_name_error', sprintf('<strong>Fehler</strong>: Bitte Vorname eingeben.' ) );
    }

    if (!frieda_custom_registration_mail_validate_name($_POST['last_name'] )) { 
        $errors->add( 'last_name_error', sprintf('<strong>Fehler</strong>: Bitte Nachname eingeben.' ) );
    }

    if (!frieda_custom_registration_mail_validate_phone($_POST['phone'] )) { 
        $errors->add( 'phone_error', sprintf('<strong>Fehler</strong>: Bitte gültige Telefonnummer eingeben.' ) );
    }
    
    if (!frieda_custom_registration_mail_validate_address($_POST['address'] )) { 
        $errors->add( 'address_error', sprintf('<strong>Fehler</strong>: Bitte Wohnadresse eingeben.' ) );
    }
    
    if (empty($_POST['terms_accepted']) or $_POST['terms_accepted'] !== "yes") { 
        $errors->add( 'terms_accepted_error', sprintf('<strong>Fehler</strong>: Bitte Nutzungsbedingungen akzeptieren.' ) );
    }
    
    if (!frieda_custom_registration_mail_validate_honigtopf($_POST['honigtopf'] )) { 
        $errors->add( 'honig_error', sprintf('<strong>Fehler</strong>: Lastenrad richtig geschrieben?' ) );
    }

    return $errors;
}


/* Perform validation of user profile edit. The save meta or show erroros
 * The actions 'personal_options_update' and 'edit_user_profile_update' could instead be used for saving,
 * but do not allow to show error in case of failed validation
 * CROSSCHECK with frieda_custom_registration_mail_registration_errors
 */
add_action( 'user_profile_update_errors', 'frieda_custom_registration_mail_user_profile_update_errors', 0, 3 );
function frieda_custom_registration_mail_user_profile_update_errors( &$errors, $update, &$user ) {

    if ( !frieda_custom_registration_mail_validate_phone($_POST['phone']) ) {
        $errors->add( 'phone', "<strong>Achtung</strong>: Ungültige Telefonnummer nicht angepasst" );
    }
    
    if ( !frieda_custom_registration_mail_validate_phone($_POST['address']) ) {
        $errors->add( 'address', "<strong>Achtung</strong>: Ungültige Wohnadresse nicht angepasst." );
    }

    if ( !current_user_can( 'edit_user', $user->ID) )
      $errors->add( 'nopermission', "<strong>Fehler</strong>: Keine Berechtigung" );

    if (!$errors->has_errors()) {
        $phone = sanitize_text_field( $_POST['phone'] );
        update_user_meta( $user->ID, 'phone', $phone );

        $address = sanitize_text_field( $_POST['address'] );
        update_user_meta( $user->ID, 'address', $address );
    }

    return $errors;

}

/* At succesful user registration, save user meta 
 * The data should have be validated by frieda_custom_registration_mail_registration_errors already
 */
add_action( 'user_register', 'frieda_custom_registration_mail_user_register' );
function frieda_custom_registration_mail_user_register( $user_id ) {
    if ( !empty( $_POST['first_name'] ) ) {
        update_user_meta( $user_id, 'first_name', sanitize_text_field( $_POST['first_name'] ) );
    }
    if ( !empty( $_POST['last_name'] ) ) {
        update_user_meta( $user_id, 'last_name', sanitize_text_field( $_POST['last_name'] ) );
    }
    if ( !empty( $_POST['phone'] ) ) {
        update_user_meta( $user_id, 'phone', sanitize_text_field( $_POST['phone'] ) );
    }
    if ( !empty( $_POST['address'] ) ) {
        update_user_meta( $user_id, 'address', sanitize_text_field( $_POST['address'] ) );
    }
}

/* Show additional user fields in user profile screen
 * for editing OWN user profile and for edition other user's profile (admin)
 */
add_action( 'show_user_profile', 'frieda_custom_registration_mail_show_user_profile' );
add_action( 'edit_user_profile', 'frieda_custom_registration_mail_show_user_profile' );
add_action( 'user_new_form', 'frieda_custom_registration_mail_show_user_profile' );
function frieda_custom_registration_mail_show_user_profile( $user ) {
    printf(<<<END
        <h3>Extrafelder </h3>

        <table class='form-table'>
            <tr>
                <th><label for='phone'>Telefonnummer</label></th>
                <td>
                    <input type='tel' name='phone' id='phone' value='%s' class='regular-text' autocomplete="tel"><br>
                </td>
            </tr>
        END, esc_attr(get_the_author_meta( 'phone', $user->ID )) );

    printf(<<<END
            <tr>
                <th><label for='address'>Adresse</label></th>
                <td>
                    <input type='text' name='address' id='address' value='%s' class='regular-text' autocomplete="address-line1"><br>
                </td>
            </tr>
        </table>
        END, esc_attr(get_the_author_meta( 'address', $user->ID )));
}

function frieda_custom_registration_mail_validate_name($name) {

    if (empty( $name ) || !empty( $name ) && trim( $name ) == '') {
        return false;
    }

    if (strlen(trim( $name ))<=1) {
        return false;
    }

    return true;
}

function frieda_custom_registration_mail_validate_phone($phone) {

    if (empty($phone)) return false;

    // We assume at least 6 digits
    preg_match_all('/\d/', $phone, $matches);
    $numberOfDigits = count($matches[0]);
    if ($numberOfDigits < 6) return false;

    return true;
}

function frieda_custom_registration_mail_validate_address($address) {

    if (empty($address)) return false;

    // We assume at least 5 digits (at least 1 for house number, at least 4 for PLZ)
    preg_match_all('/\d/', $address, $matches);
    $numberOfDigits = count($matches[0]);
    if ($numberOfDigits < 5) return false;

    // We assume at least 4 letters (at least 2 for street and at least 2 for city)
    $lettersOnly = preg_replace('/[^a-zA-ZäöüßÄÖÜ]+/u', '', $address);
    $numberOfLetters = mb_strlen($lettersOnly, 'UTF-8');
    if ($numberOfLetters < 4) return false;

    return true;
}

function frieda_custom_registration_mail_validate_honigtopf($honigtopf) {
    return strtoupper($honigtopf) === 'LASTENRAD';
}

// Cleanup to user admin page by adding CSS hiding color selection, language ...
add_action('admin_enqueue_scripts', 'frieda_custom_registration_mail_add_admin_style');
function frieda_custom_registration_mail_add_admin_style() {

    $currentScreen = get_current_screen();

    if ( $currentScreen->id === "profile") {
        // user editing own profile: cleanup
        wp_enqueue_style('profile-edit-cleanup', plugins_url( 'profilecleanup.css' , __FILE__ ) );
    } else if ( $currentScreen->id === "user-edit") {
        // user editing other user's profile: don't cleanup
    }
}

