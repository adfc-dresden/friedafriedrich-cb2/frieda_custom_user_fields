<?php
/*
 * Plugin Name: Frieda & Friedrich Custom User Registration Mail
 * Author: Nils Larsen
 */

add_filter( 'wp_new_user_notification_email', 'custom_wp_new_user_notification_email', 10, 3 );

function custom_wp_new_user_notification_email( $wp_new_user_notification_email, $user, $blogname ) {
    $key = get_password_reset_key( $user );
    $firstname = $user->first_name ;
    
    $pwreseturl = network_site_url("wp-login.php?action=rp&key=$key&login=" . rawurlencode($user->user_login), 'login');
    
    $message = get_option('new_user_notification_email_body');
    $message = str_replace("{{user:first_name}}", $firstname, $message);
    $message = str_replace("{{pwreseturl}}", $pwreseturl, $message);

    $wp_new_user_notification_email['message'] = $message;

    $wp_new_user_notification_email['subject'] = get_option('new_user_notification_email_subject');

    $fromHeaders = get_option('new_user_notification_email_from');

    $wp_new_user_notification_email['headers'] = array('Content-Type: text/html; charset=UTF-8',
                                                       "From: $fromHeaders");

    return $wp_new_user_notification_email;
}

function frieda_custom_registration_mail_admin_menu() {
    add_options_page(
        "Custom User Field",
        "Custom User Field",
        'manage_options', // capability
        'custom_user_fields', // menu slug
        'frieda_custom_registration_mail_admin_page_contents'  // function to generate settings page
    );
}
add_action( 'admin_menu', 'frieda_custom_registration_mail_admin_menu' );

function frieda_custom_registration_mail_admin_page_contents() {
    ?>
    <h1>Frieda Custom User Fields Settings </h1>
    <form method="POST" action="options.php">
    <?php
    settings_fields( 'custom_user_fields' );
    do_settings_sections( 'custom_user_fields' );
    submit_button();
    ?>
    </form>
    <?php
}

add_action( 'admin_init', 'frieda_custom_registration_mail_init' );

function frieda_custom_registration_mail_init() {

    add_settings_section(
        'sample_page_setting_section',
        __( 'Custom settings', 'my-textdomain' ),
        'frieda_custom_registration_mail_setting_section_callback_function', // callback function to generate section intro text
        'custom_user_fields'
    );

    add_settings_field(
       'new_user_notification_email_from',
       'New User Registration E-Mail From Header',
       'markup_new_user_notification_email_from', // callback function to generate markup for single input field
       'custom_user_fields',
       'sample_page_setting_section'
    );

    register_setting( 'custom_user_fields', 'new_user_notification_email_from' );

    add_settings_field(
       'new_user_notification_email_subject',
      'New User Registration E-Mail Subject',
       'markup_new_user_notification_email_subject', // callback function to generate markup for single input field
       'custom_user_fields',
       'sample_page_setting_section'
    );

    register_setting( 'custom_user_fields', 'new_user_notification_email_subject' );

    add_settings_field(
       'new_user_notification_email_body',
      'New User Registration E-Mail Body',
       'markup_new_user_notification_email_body', // callback function to generate markup for single input field
       'custom_user_fields',
       'sample_page_setting_section'
    );

    register_setting( 'custom_user_fields', 'new_user_notification_email_body' );
}

function frieda_custom_registration_mail_setting_section_callback_function() {
    echo <<<EOF
    <p>Customize the e-mails regarding user registration here (Frieda Custom User Fields Plugin). Use the following template tags:</p>
    <ul>
    <li>{{pwreseturl}}: The Password reset URL
    <li>{{user:first_name}}: First name of the user
    </ul>
    EOF;
}

function markup_new_user_notification_email_from() {
    ?>
    <input type="text" id="new_user_notification_email_from" name="new_user_notification_email_from" value="<?php echo esc_attr(get_option( 'new_user_notification_email_from' )); ?>">
    <?php
}

function markup_new_user_notification_email_subject() {
    ?>
    <input type="text" id="new_user_notification_email_subject" name="new_user_notification_email_subject" value="<?php echo esc_attr(get_option( 'new_user_notification_email_subject' )); ?>">
    <?php
}

function markup_new_user_notification_email_body() {
    ?>
    <textarea id="new_user_notification_email_body" name="new_user_notification_email_body" cols="60" rows="10" value=""><?php echo esc_textarea(get_option( 'new_user_notification_email_body' )); ?></textarea>
    <?php
}

