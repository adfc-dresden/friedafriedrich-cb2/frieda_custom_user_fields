<?php
/*
 * Plugin Name: Frieda & Friedrich Style Profile Editor
 * Author: Nils Larsen
 */

// Add "Return to website" button to profile edit page of a user
function custom_profile_return_script() {
    $current_screen = get_current_screen();
    
    /* This is the original HTML output from Wordpress to add the return button to:
       <p class="submit"><input type="submit" name="submit" id="submit" class="button button-primary" value="Profil aktualisieren"></p>
    */
    
    // Check if we are on an edit user profile screen and the current user is not an admin
    if ($current_screen && $current_screen->base === 'profile') {
        ?>
        <script>
            document.addEventListener('DOMContentLoaded', function () {
                var submitButton = document.getElementById('submit');

                if (submitButton) {
                    var space = document.createTextNode(' ');
                    submitButton.parentNode.appendChild(space);

                    var returnButton = document.createElement('a');
                    returnButton.href = '<?php echo esc_url(home_url()); ?>';
                    returnButton.className = 'button';
                    returnButton.textContent = 'Zurück zur Website';
                    submitButton.parentNode.appendChild(returnButton);

                }
            });
        </script>
        <?php
    }
}

add_action('admin_footer', 'custom_profile_return_script');
