<?php
/*
 * Plugin Name: Frieda & Friedrich Redirect to Main at Login
 * Author: Nils Larsen
 */

add_filter( 'login_redirect', 'frieda_login_redirect', 10, 3 );
function frieda_login_redirect( $redirect_to, $requested_redirect_to, $user ) {

    if ( is_a( $user, 'WP_User' ) && !is_wp_error( $user ) ) {
        // If the user is not an administrator and no specific redirect URL is requested, redirect to the main page
		//die($requested_redirect_to);
        if ( !current_user_can( 'administrator' ) && $requested_redirect_to === admin_url() ) {
			
            $redirect_to = home_url();
        }
    }
    return $redirect_to;
}

?>